package cap.wesantos.localizacao;

import cap.wesantos.localizacao.domain.entity.Cidade;
import cap.wesantos.localizacao.domain.service.CidadeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LocalizacaoApplication implements CommandLineRunner {

	@Autowired
	private CidadeService cidadeService;

	@Override
	public void run(String... args) throws Exception {
		System.out.println("Inicialização...");
		Cidade cidade = new Cidade(1L, "São Paulo", 1000L);
		cidadeService.listarCidadesSQLProjection();
	}

	public static void main(String[] args) {
		SpringApplication.run(LocalizacaoApplication.class, args);
	}

}
