package cap.wesantos.localizacao.domain.repository.spec;

import cap.wesantos.localizacao.domain.entity.Cidade;
import org.springframework.data.jpa.domain.Specification;

public abstract class CidadeSpec {
    /*
        https://en.wikibooks.org/wiki/Java_Persistence/Criteria#Criteria_API
        https://www.objectdb.com/java/jpa/query/criteria
     */
    public static Specification<Cidade> nomeEqual(String nome) {
        return (root, query, criteriaBuilder) ->  criteriaBuilder.equal(root.get("nome"), nome);
    }

    public static Specification<Cidade> habitantesGreaterThan(Long habitantes) {
        return (root, query, criteriaBuilder) ->  criteriaBuilder.greaterThan(root.get("habitantes"), habitantes);
    }

    public static Specification<Cidade> propEquals(String prop, Object value) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get(prop), value);
    }
}
