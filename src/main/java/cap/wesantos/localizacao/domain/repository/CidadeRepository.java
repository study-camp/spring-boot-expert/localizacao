package cap.wesantos.localizacao.domain.repository;

import cap.wesantos.localizacao.domain.entity.Cidade;
import cap.wesantos.localizacao.domain.repository.projection.CidadeProjection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CidadeRepository extends JpaRepository<Cidade, Long>, JpaSpecificationExecutor<Cidade> {
    /*  https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#repository-query-keywords
     */

    List<Cidade> findByNome(String nome);

    @Query(value = "SELECT c.id_cidade id, c.nome FROM tb_cidade c WHERE c.nome = :nome", nativeQuery = true)
    List<CidadeProjection> findByNomeNativa(@Param("nome") String nome);

    @Query("select c from Cidade c where upper(c.nome) like upper(?1)")
    Page<Cidade> findByNomeLike(String nome, Pageable pageable);

    @Query("select c from Cidade c where upper(c.nome) like upper(?1)")
    List<Cidade> findByNomeLike(String nome, Sort sort);

    // Começando com ...
    List<Cidade> findByNomeStartingWith(String nome);

    // Terminando com ...
    List<Cidade> findByNomeEndingWith(String nome);

    // Que contenha ...
    List<Cidade> findByNomeContaining(String nome);

    List<Cidade> findByHabitantes(Long habitantes);

    // qtd_habitantes <
    List<Cidade> findByHabitantesLessThan(Long habitantes);

    // qtd_habitantes <=
    List<Cidade> findByHabitantesLessThanEqual(Long habitantes);

    List<Cidade> findByHabitantesIsGreaterThan(Long habitantes);

    // combinar queries
    List<Cidade> findByHabitantesIsGreaterThanAndNomeLikeIgnoringCase(Long habitantes, String nome);
}