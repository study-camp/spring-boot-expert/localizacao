package cap.wesantos.localizacao.domain.repository.projection;
/*
    https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#projections
    https://www.baeldung.com/spring-data-jpa-projections
 */
public interface CidadeProjection {
    public Long getId();
    public String getNome();
}
