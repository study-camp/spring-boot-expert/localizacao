package cap.wesantos.localizacao.domain.service;

import cap.wesantos.localizacao.domain.entity.Cidade;
import cap.wesantos.localizacao.domain.repository.CidadeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.List;

import static cap.wesantos.localizacao.domain.repository.spec.CidadeSpec.*;

@Service
public class CidadeService {
    @Autowired
    private CidadeRepository repository;

    @Transactional
    public void salvarCidade() {
        var cidade = new Cidade(1L, "São Paulo", 12345678L);
        repository.save(cidade);
    }

    public List<Cidade> filtroDinamico(Cidade cidade) {
        ExampleMatcher matcher = ExampleMatcher
                .matching()
                .withIgnoreCase("nome")
                .withStringMatcher(ExampleMatcher.StringMatcher.STARTING);
        Example<Cidade> example = Example.of(cidade, matcher);
        return repository.findAll(example);
    }

    public void listaCidadeByNomeSpec() {
        repository.findAll(
                nomeEqual("Fortaleza")
                .and(habitantesGreaterThan(1000L))
                .or(propEquals("id", 2))
        ).forEach(System.out::println);
    }

    public void filtroDinamicoSpec(Cidade cidade) {
        Specification<Cidade> filtroDinamico = Specification.where((root, query, cb) -> cb.conjunction());
        if (cidade.getId() != null) {
            filtroDinamico = filtroDinamico.and(propEquals("id", cidade.getId()));
        }

        if (StringUtils.hasText(cidade.getNome())) {
            filtroDinamico = filtroDinamico.and(nomeEqual(cidade.getNome()));
        }

        if (cidade.getHabitantes() != null) {
            filtroDinamico = filtroDinamico.and(habitantesGreaterThan(cidade.getHabitantes()));
        }
        repository.findAll(filtroDinamico).forEach(System.out::println);
    }

    public void listarCidadesSQLProjection() {
        repository.findByNomeNativa("São Paulo")
                .stream()
                .map(projection -> new Cidade(projection.getId(), projection.getNome(), null))
                .forEach(System.out::println);
    }

    public void listarCidade() {
        Pageable pageable = PageRequest.of(1, 5);
        repository.findAll(pageable).forEach(System.out::println);
    }

    public void listarCidadePorNome() {
        System.out.println("==== Listar cidades por nome começados com S ====");
        var cidades = repository.findByNomeStartingWith("S");
        cidades.forEach(System.out::println);
    }

    public void listarCidadePorNomeLike() {
        System.out.println("==== Listar cidades por nome que possua a letra a ====");
        var cidades = repository.findByNomeLike("%a%", Sort.by("habitantes"));
        cidades.forEach(System.out::println);
    }

    public void listarCidadePorHabitantes() {
        System.out.println("==== Listar cidades por Quantidade de Habitantes ====");
        var cidades = repository.findByHabitantes(7000000L);
        cidades.forEach(System.out::println);
    }

    public void listarCidadePorHabitantesMenosQue() {
        System.out.println("==== Listar cidades por Quantidade de Habitantes Menos Que ... ====");
        var cidades = repository.findByHabitantesLessThan(7000000L);
        cidades.forEach(System.out::println);
    }

    public void listarCidadePorHabitantesMaiorQueENomeLike() {
        System.out.println("==== Listar cidades por Quantidade de Habitantes Mais que Que ... e contenha %f% no nome ====");
        var cidades = repository.findByHabitantesIsGreaterThanAndNomeLikeIgnoringCase(7000000L, "%f%");
        cidades.forEach(System.out::println);
    }
}
