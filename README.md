# Aprofundando em Spring Boot JPA
Módulo 12 do curso "Spring Boot Expert: JPA, RESTFul API, Security, JWT e Mais"

O objetivo do módulo é focar nas funcionalidades do Spring Data JPA, portanto não possui interface ou endpoints, todos os testes são feitos na classe *LocalizacaoApplication.java*. 
### Springboot v2.6.14

Dependências
* Spring Boot DevTools
* Lombok
* Spring Data JPA
* H2 Database
